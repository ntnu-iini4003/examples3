#include <cctype>
#include <iostream>
#include <string>

using namespace std;

int main() {
  string word;

  cout << "Skriv ord, et på hver linje, avslutt med \"x\":" << endl;

  // Vi leser ordet inn i et string-objekt
  cin >> word;

  // Vi kan bruke sammenligningsoperatorene for å sammenligne
  // innholdet i streng-objektene. (For å sammenligne strenger
  // som er lagret i char-tabeller må vi bruke strcpy(), dersom
  // vi bruker operatorene da, vil vi sammenligne adressene, og
  // det er sjelden meningen!)
  while (word != "x" && word != "X") {
    cout << word;
    for (size_t i = 0; i < word.length(); i++) {
      word[i] = toupper(word[i]);
    }

    cout << " Omformet: " << word << endl;
    cin >> word;
  }
}
