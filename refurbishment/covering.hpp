#pragma once

#include "surface.hpp"
#include <string>

using namespace std;

const double limit = 0.02; // grense for å beregne en bredde ekstra

class Covering {
public:
  Covering(string name_, double price_, double width_);
  string get_name() const { return name; }
  double get_price() const { return price; }
  double get_width() const { return width; }

  /*
  * Skal beregne antall meter som trengs til en flate.
  * Teppet legges alltid på tvers av golvets lengde.
  * For å finne ut om det er billigere å legge teppet andre veien
  * må klienten bytte om lengde og bredde i den flaten som
  * sendes inn som argument.
  */
  double get_meters(const Surface &surface) const;
  double get_price(const Surface &surface) const;

private:
  string name;  // identifiserer belegget
  double price; // pris per meter belegg (i lengden)
  double width; // meter
};
