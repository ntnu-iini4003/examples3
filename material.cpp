#include <iostream>
#include <string>

class Material {
public:
  Material(const std::string &name_, double price_);
  const std::string &get_name() const { return name; }
  double get_price() const { return price; }
  void print() const;

private:
  std::string name; // identifiserer materialet
  double price;     // pris pr enhet
};

class Covering : public Material {
public:
  // Prisen er per meter belegg (i lengden)
  Covering(const std::string &name_, double price_, double width_);
  double get_width() const { return width; }
  void print() const;

private:
  double width; // meter
};

using namespace std;

Material::Material(const string &name_, double price_) : name(name_), price(price_) {}

void Material::print() const {
  cout << endl
       << "Navn:          " << name << endl
       << "Pris pr enhet  " << price << endl;
}

Covering::Covering(const string &name_, double price_, double width_)
    : Material(name_, price_),
      width(width_) {}

void Covering::print() const {
  Material::print();
  cout << "For belegg:    " << endl
       << "Bredde:        " << width << endl;
}

int main() {
  Covering covering("Super Duper Dux", 433.50, 4);
  // get_name() og get_price() arves til subklassene:
  cout << "Navn: " << covering.get_name() << ", pris: " << covering.get_price() << endl;
  // Objektet bruker sin egen print()-funksjon (og ikke den som er arvet)
  covering.print();
}

/*
Navn: Super Duper Dux, pris: 433.5

Navn:          Super Duper Dux
Pris pr enhet  433.5
For belegg:    
Bredde:        4
*/
